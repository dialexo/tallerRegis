﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace tallerRegis
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();       
        }
        async private void registro(object sender, EventArgs e)
            {
            if (string.IsNullOrEmpty(entrynombre.Text) || string.IsNullOrEmpty(entryapellido.Text) || string.IsNullOrEmpty(entrytelefono.Text) || string.IsNullOrEmpty(entryemail.Text))
            {
               

                await DisplayAlert("Error", "Debe Escribir Datos completos", "Ok");

            }
            else
            {
                await Navigation.PushAsync(new Page1());

                
            }
        }
    }
}
